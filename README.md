# Currencies web application

#### Open main page (8th step of running guide ) in browser to see 'UAH-USD' rates or get ability to add new.
#### Also you can add rates for other currencies. 
#### For this: go to '/usd-eur', for example. You will see same form as on 'UAH-USD' page.
#### You can add new rates, after this you will be redirected to respective currency page.

## Running locally:

1) Clone repo
```
git clone https://bitbucket.org/aperekhozhuk/currencies
```
2) Cd into repo folder
```
cd currencies/
```
3) Setup virtual environment called 'venv' and activate it
Install pip, if you don't have
```
pip install virtualenv    # Install virualenv package, if needed
virtualenv venv
source venv/bin/activate
```
4) Installing dependencies
```
pip install -r requirements.txt
```
5) Make migrations to create your local database
```
python manage.py migrate
```
6) Running app
```
python manage.py runserver 8080   # or other avaliable port
```
7) Open in browser localhost:8080 # or other port which defined on previous step

8) Create superuser (optionally) for log in django-admin panel
```
python manage.py createsuperuser
```
