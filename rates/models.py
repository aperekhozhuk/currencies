from django.db import models

class Rate(models.Model):
    currency = models.CharField(max_length=7)   # 'uah-usd' , 'eur-rub' etc
    rate = models.FloatField()
    date = models.DateField()                   # date of publishing

    def __str__(self):
        return "{}:{}, {}".format(self.currency, self.rate, self.date)
