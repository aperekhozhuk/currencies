from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages

from .models import Rate


# By default we show rates for UAH-USD currency. Also we show form for adding rates for respective currency
def index(request, currency = 'uah-usd'):
    rates = Rate.objects.filter(currency = currency).order_by('date')
    return render(request, 'rates/index.html', {'rates' : rates, 'currency': currency})

# We retrieve currency from hidden value, which renders in 'index.html' by 'index' view,
# so we shouldn't demand from user to manually input this
def new(request):
    date = request.POST['date']
    rate = request.POST['rate']
    currency = request.POST['currency']
    r = Rate(rate = rate, date = date, currency = currency)
    # Handling validation errors
    try:
        r.save()
    except Exception:
        messages.error(request, 'Bad input data')
    return HttpResponseRedirect(reverse('rates:view', kwargs={'currency': currency}))
