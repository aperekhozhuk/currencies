from django.urls import reverse
from django.test import TestCase

from .models import Rate

def create_rate(currency, rate, date):
    return Rate.objects.create(currency=currency, rate = rate, date = date)

class RateModelTests(TestCase):
    # Checking, that already created rate will disply in browser
    def test_rate_add_success(self):
        currency = 'uah-usd'
        rate = 0.4
        date = '2020-01-03'
        create_rate(currency = currency, rate = rate, date = date)
        response = self.client.get(reverse('rates:index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['rates'], ['<Rate: {}:{}, {}>'.format(currency, rate, date)])

    # Checking, that user will not see any rates, if no one was added
    def test_no_rates_yes(self):
        response = self.client.get(reverse('rates:index'))
        self.assertQuerysetEqual(response.context['rates'], [])
