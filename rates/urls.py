from django.urls import path, re_path
from .views import index, new

app_name = 'rates'
urlpatterns = [
    # Path for homepage, its avaliable just by '/'
    path('', index, name = 'index'),
    # Path for specially currency page, its avaliable by '/eur-usd', for example
    re_path(r'^(?P<currency>[a-zA-z]{2,3}-[a-zA-z]{2,3})/$', index, name = 'view'),
    path('new/', new, name ='new'),
]